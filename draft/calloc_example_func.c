#include <stdio.h>

#include <stdlib.h>
void re_size(int **, int);


int main() {
  int * p = NULL;
  int ii=0;
  /*Solicitando espacio para 15 valores enteros*/
  p = (int *) malloc(15 * sizeof(int));
  /*Carga de datos*/
  for (ii = 0; ii < 15; ii++) 
  {
    *(p + ii) = ii;
  }

  printf("Start address %p\n", p);
  /*Impresion de datos*/
  for (ii = 0; ii < 15; ii++) 
  {
    printf("%d\n", *(p + ii));
  }
  
  re_size(&p,25);

  /*Carga de nuevos datos*/
  for (ii = 0; ii < 10; ii++) 
  {
    *(p + ii + 15) = ii;
  }


  printf("Start address %p\n", p);
 /*Impresion total*/
  for (ii = 0; ii < 25; ii++) 
  {
    printf("%d\n", *(p + ii));
  }
 /*Impresion de posiciones de memoria */
  for (ii = 0; ii < 25; ii++) 
  {
    printf("%p\n", (p + ii));
  }
  free(p);
  return (0);
}


void re_size(int **p, int new_dim)
{
    /*Redimension de la memoria*/
    *(p) = (int *) realloc(*(p), new_dim * sizeof(int));
}