/*--- LIBRERIAS ---*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TAM 15

/*--- ESTRUCTURAS ---*/
struct datosCancion
{
    char artista[15];
    char titulo[15];
    int duracion;
    float tam_kb;   
};

/*--- FUNCION PRINCIPAL MAIN ---*/
int main(int argc, char const *argv[])
{
    struct datosCancion cancion; // declaracion de la estructura (comienza a ocupar memoria)
    /* Carga de datos */
    printf("Ingrese el titulo de cancion: ");
    scanf("%s",cancion.titulo);
    printf("Ingrese el nombre del Artista: ");
    scanf("%s",cancion.artista);
    printf("Ingrese la duracion de la cancion en segundos: ");
    scanf("%d", &cancion.duracion);
    printf("Ingrese el tamaño en KB de la cancion: ");
    scanf("%f", &cancion.tam_kb);
    /* Impreseion de la estructura ("datos de cancion") */
    printf("\nDatos de la cancion:\n"
            "Artista: %s\n"
            "Titulo: %s\n"
            "Duracion: %d seg\n"
            "Tamaño: %.2f KB\n",
            cancion.artista, cancion.titulo, cancion.duracion, cancion.tam_kb);
    return 0;
}
