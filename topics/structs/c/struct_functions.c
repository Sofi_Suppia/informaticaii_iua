#include <stdio.h>
struct measurement
{
    float temp;
    float hum;
    int id;
};

/*Prototipo de la funcion*/
void print_members(struct measurement);


int main()
{  
    struct measurement m;
    printf("Ingrese la temperatura\n");
    scanf("%f",&m.temp);
    printf("Ingrese la humedad\n");
    scanf("%f",&m.hum);
    m.id=10;
    /*Llamada a la funcion*/
    print_members(m);

}



void print_members(struct measurement m)
{
    printf("Temperatura \t %f\n",m.temp);
    printf("Humedad \t %f\n",m.hum);
    printf("Id \t \t %d\n",m.id);
}



