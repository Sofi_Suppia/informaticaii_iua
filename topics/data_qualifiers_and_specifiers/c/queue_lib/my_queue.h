#ifndef MY_QUEUE_H  
#define MY_QUEUE_H
struct Node 
{
  int data;
  struct Node * next;
};
#endif
