
#include<stdio.h>
int mul(int n1, int n2)
{
    return (n1*n2);
}
void main(void)
{   int res;
    int (*ptr_to_func)(int,int);
    ptr_to_func = mul;
    res = (*ptr_to_func)(10,5);
    printf("10*5 = %d\n",res);
    res = (*ptr_to_func)(100,10);
    printf("100*10 = %d\n",res);
}