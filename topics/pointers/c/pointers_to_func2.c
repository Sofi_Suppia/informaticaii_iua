#include<stdio.h>

void hola_mundo(void);
void chau_mundo(void);

int main(void)
{
   void (*ptr_to_func)();  //Puntero a funcion
   ptr_to_func = hola_mundo; //Puntero a hola mundo
   (*ptr_to_func)();   //Llamando
   ptr_to_func = chau_mundo;
   (*ptr_to_func)();  
   return(0);
}


void hola_mundo(void)
{
   printf("Hello IUA\n");
}

void chau_mundo(void)
{
   printf("ByeBye IUA\n");
}
