# include <stdio.h>
# include <stdlib.h>

/*Definicion de la esctructura*/
struct node
{
    int data;
    struct node *link;
};

int menu (void);
void push(struct node **,struct node **, int );
void pop(struct node **,struct node **);
void print(struct node *);

int menu (void)
{
    int op;
    do
    {
        printf("1-Agregar un nodo\n");
        printf("2-Borrar un nodo\n");
        printf("3-Imprimir cola\n");
        printf("4-Salir\n");
        scanf("%d",&op);
    }while((op<1)||(op>4));
    return(op);
}


void push(struct node **front,struct node **rear, int dato)
{
    struct node *temp;
    temp=(struct node *)malloc(sizeof(struct node));
    if(temp==NULL)
    {   printf("No Memory available\n");
        exit(0);
    }
    temp->data = dato;
    temp->link=NULL; 
    if(*rear == NULL)  /*Insercion del primer nodo*/
    {
        *rear = temp;
        *front = *rear;
    }
    else /*Insercion del resto de los nodo*/
    {
        (*rear)->link = temp;
        *rear = temp;
    }
}

void pop(struct node **front,struct node **rear)
{
    struct node *temp;
    if((*front == *rear) && (*rear == NULL))
    {
        printf("Vacia\n");
        exit(0);
    }
    temp = *front;
    *front = (*front)->link;
    if(*rear == temp)
    {
        *rear = (*rear)->link;
    }
    
    free(temp);
}






void print(struct node *front)
{
    struct node *temp   =NULL;
    /*Impresion de toda la FIFO*/
    temp=front;
    while(temp!=NULL)
    {
        printf("%d->",temp->data);
        temp=temp->link;
    }
    printf("\n");
}



void main()
{
    struct node *front  =NULL;
    struct node *rear   = NULL;
    struct node *temp   =NULL;
    int dato;
    int value;
    int op;
    do
    {   op=menu();
        switch(op)
        {
            case 1:
                printf("Ingrese el dato a insertar\n");
                scanf("%d",&dato);
                push(&front,&rear,dato);
                break;
            case 2:
                pop(&front,&rear);
                break;
            case 3:
                print(front);
                break;
        }
    }while(op!=4);
    
}
