/* Variables globales*/
#include <stdio.h>
void func2(void);
int a=1, b=2;
int main () 
{
  /* Variables locales a Main */
  a=10, b=20;
  printf ("Impresion en main\n"); 
  printf ("Valores de a = %d, b = %d\n", a, b); 
  func2();
  printf ("Impresion en main\n"); 
  printf ("Valores de a = %d, b = %d\n", a, b); 
  return 0;
}

void func2 () 
{
  a++;
  b++;
  printf ("Impresion en Func2\n"); 
  printf ("Valores de a = %d, b = %d\n", a, b); 
}